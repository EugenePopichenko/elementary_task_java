package com.softserve.edu.task1.view;

import com.softserve.edu.task1.model.ChessBoardAppExceptions.ChessBoardAppException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by Eugene_P on 2016-01-20.
 */
public class InputTest {
    @DataProvider
    public String[][] IncomeExceptioncallingParamsToApp1() {
        return new String[][]{
                {"0 5 3"},
                {"0 5"},
                {"-3 5"},
                {"-3 -5"},
                {"9 f"}
        };
    }

    @Test(expectedExceptions = Exception.class, // doesn't work with ChessBoardExceptions
    dataProvider = "IncomeExceptioncallingParamsToApp1")
    public void testValidate(String[] args) throws Exception {
        new Input().validate(args);
    }
}