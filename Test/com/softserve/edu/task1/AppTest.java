package com.softserve.edu.task1;

import com.softserve.edu.task1.model.ChessBoardAppExceptions.ChessBoardAppException;
import com.softserve.edu.task1.model.ChessBoardAppExceptions.NotAPositiveIntegerException;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * tests for task1
 */
public class AppTest {

    @DataProvider
    public String[][] IncomeValidParamsToApp1() {
        String callString = "java -cp out\\production\\elementary_task_java" +
                " com.softserve.edu.task1.App";

        return new String[][]{
                {callString + " " + "3 5"}
        };
    }

    @Test(dataProvider = "IncomeValidParamsToApp1")
    public void testMainValidParams(String incomeCmdStr) throws Exception {
        Runtime rt = Runtime.getRuntime();
        Process process = rt.exec(incomeCmdStr);

        String actualStr = "";
        try (BufferedReader reader =
                     new BufferedReader(
                             new InputStreamReader(
                                     process.getInputStream()))) {
            int tmp;
            while ((tmp = reader.read()) != -1) {
                actualStr += (char) tmp;
            }
        }
//        String endOfOutput = "\n\r\n";
        actualStr = actualStr.substring(0, actualStr.length() - 3);

        String expectedStr3_5 =
                "* *\n" +
                " * \n" +
                "* *\n" +
                " * \n" +
                "* *";
        Assert.assertEquals(actualStr, expectedStr3_5);
    }

    @DataProvider
    public String[][] IncomeInvalidParamsToApp1() {
        String callString = "java -cp out\\production\\elementary_task_java" +
                " com.softserve.edu.task1.App";

        return new String[][]{
                {callString + " " + "-3 -5"},
                {callString + " " + "9 f"}
        };
    }

    @Test(dataProvider = "IncomInvalidParamsToApp1")
    public void testMainInvalidParams(String incomeCmdStr) throws Exception {
        String cmdString = incomeCmdStr;
        Runtime rt = Runtime.getRuntime();
        Process process = rt.exec(cmdString);

        Assert.fail();
    }

//    @DataProvider
//    public String[][] IncomeExceptioncallingParamsToApp1() {
//        return new String[][]{
//                {"0 5 3"},
//                {"0 5"},
//                {"-3 5"},
//                {"-3 -5"},
//                {"9 f"}
//        };
//    }
//
//    @Test(dataProvider = "IncomeExceptioncallingParamsToApp1",
//    expectedExceptions = Exception.class) // doesnt work with amy of ChessBoardAppException s
//    public void testMainExceptioncallingParams(String incomeCmdStr) throws Exception {
//        Class c = Class.forName("com.softserve.edu.task1.App");
//        c.getDeclaredMethod("main").invoke(incomeCmdStr);
//    }

}