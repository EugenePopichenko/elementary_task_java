package com.softserve.edu.task8;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 09.01.2016.
 */
public class FibonacciInRange {
    public static List<Integer> generate(int[] range) {

        List<Integer> row = new ArrayList<>();

        int f1 = 0;
        int f2 = 1;
        int f = 0;
        int lower = range[0];
        int upper = range[1];

        for (int i = lower; i < upper; i++) {
            if (f <= upper) {
                if (f >= lower) {
                    row.add(f);
                }
                f1 = f2;
                f2 = f;
                f = f1 + f2;
            }
        }
        return row;
    }
}
