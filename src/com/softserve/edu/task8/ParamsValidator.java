package com.softserve.edu.task8;

import java.util.Arrays;

public class ParamsValidator {

    public static int[] validate(String[] stringArgs) {
        // Help, no params.
        if (stringArgs.length == 0) {
            System.out.println("\n===Help===\n" +
                    "call the \"com.softserve.edu.task8.App x y\".\n" +
                    "where \"x\" is an integer, number to start fibonacci row with;\n" +
                    "      \"y\" is an integer, number to print fibonacci row up to;");
            System.exit(0);
        }

        // more than two params
        if (stringArgs.length != 2) {
            System.out.println("\n===Wrong number of params===\n" +
                    "give the TWO integer as a params, to run the App\n" +
                    "where the \"first\" is an integer, number to start fibonacci row with;\n" +
                    "      the \"second\" is an integer, number to print fibonacci row up to;");
            System.exit(0);
        }

        // if isInteger and positive
        int[] intArgs = new int[2];
        try {
            intArgs[0] = Integer.parseInt(stringArgs[0]);
            intArgs[1] = Integer.parseInt(stringArgs[1]);
            if ((intArgs[0] < 0) || (intArgs[1] < 0)) {
                throw new NumberFormatException();
            }
        } catch (NumberFormatException e) {
            System.out.println("\n***NumberFormatException***\ngive two POSITIVE INTEGER as a param, to run the App");
            System.exit(0);
        }

        // sort
        Arrays.sort(intArgs);

        return intArgs;
    }
}
