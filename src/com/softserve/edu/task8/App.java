package com.softserve.edu.task8;

import java.util.List;

/**
 * Created by User on 09.01.2016.
 */
public class App {
    public static void main(String[] args) {

        List<Integer> fibRow = FibonacciInRange.generate(ParamsValidator.validate(args));

        if (fibRow.size() < 0) {
            System.out.println("there is no any Fib num in the range u've given");
        } else {
            String fibRowStr = fibRow.toString();
            System.out.println(fibRowStr.substring(1, fibRowStr.length() - 1));
        }
    }
}
