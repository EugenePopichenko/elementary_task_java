package com.softserve.edu.task3;

import java.util.Scanner;

/**
 * Created by User on 09.01.2016.
 */
public class TriangleInputValidator {
    public static String[] getInputAndValidate() {

        String[] params = new String[4];

        Scanner scn = new Scanner(System.in);
        String scannedStr = "";

        while (scannedStr.length() == 0) {
            scannedStr = scn.nextLine();
            if (scannedStr.length() == 0) {
                System.out.println("Hey! type triangle params like \"name, side, side, side\"\n" +
                        "name - string;\n" +
                        "side(s) - double, bigger than \"0\", length valid for triangle\n" +
                        "\",\" - separator");
                scannedStr = scn.nextLine();
            }
            if (scannedStr.length() != 0) {
                try {
                    params[0] = scannedStr.trim().split(",")[0].trim().toLowerCase();
                    params[1] = scannedStr.trim().split(",")[1].trim().toLowerCase();
                    params[2] = scannedStr.trim().split(",")[2].trim().toLowerCase();
                    params[3] = scannedStr.trim().split(",")[3].trim().toLowerCase();

                    double num;
                    double sumOfOtherSides;
                    for (int i = 1; i < params.length; i++) {
                        num = Double.parseDouble(params[i]);
                        switch (i) {
                            case 1:
                                sumOfOtherSides = Double.parseDouble(params[2]) + Double.parseDouble(params[3]);
                                break;
                            case 2:
                                sumOfOtherSides = Double.parseDouble(params[1]) + Double.parseDouble(params[3]);
                                break;
                            case 3:
                                sumOfOtherSides = Double.parseDouble(params[1]) + Double.parseDouble(params[2]);
                                break;
                            default:
                                sumOfOtherSides = 0;
                        }
                        if ((num <= 0) || (num >= sumOfOtherSides)) {
                            scannedStr = "";
                        }
                    }
                } catch (Exception e) {
                    scannedStr = "";
                }
            }
        }
        return params;
    }
}
