package com.softserve.edu.task3;

import com.softserve.edu.task0.Helpable;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * Created by User on 09.01.2016.
 */
public class App {
    public static void main(String[] args) {

        Helpable triangleHelp = ()->{
            System.out.println("This is Help\nFolow the instructions below...");
        };
        triangleHelp.displayHelp();

        Map<Double, String> mapSquareName = new TreeMap<>((o1, o2) -> o2.compareTo(o1));
        boolean toContinue = true;

        while (toContinue) {
            System.out.println("\nEnter the triangle like \"name, side, side, side\"");

            Triangle triangle = new Triangle(TriangleInputValidator.getInputAndValidate());
            Double triangleSquare = triangle.getSquare();
            String triangleName = triangle.getName();

            mapSquareName.put(triangleSquare, triangleName);

            toContinue = App.askForContinuing();
        }

        System.out.println(sortedOutputOfTriangles(mapSquareName));
    }

    private static boolean askForContinuing() {
        boolean toContinue;
        System.out.println("Please, type \"Yes\" or \"Y\" to add one more triangle...otherwise, the App will result and exit");
        Scanner scn = new Scanner(System.in);
        String scannedLine = scn.nextLine().trim();
        if (scannedLine.equalsIgnoreCase("yes") || scannedLine.equalsIgnoreCase("y")) {
            toContinue = true;
        } else {
            toContinue = false;
        }
        return toContinue;
    }

    private static String sortedOutputOfTriangles(Map<Double, String> mapSquareName) {

        String output = "============= Triangles list: ===============\n";

        int i = 0;
        for (Map.Entry<Double, String> entry : mapSquareName.entrySet()) {
            output += i++ + ". [" + entry.getValue() + "]: " + entry.getKey() + " cm\n";
        }

        return output;
    }
}

