package com.softserve.edu.task3;

/**
 * Created by User on 09.01.2016.
 */
public class Triangle {
    private String name;
    private double sideOne;
    private double sideTwo;
    private double sideThree;

    public Triangle(String[] userinput) {
        this.name = userinput[0].substring(0, 1).toUpperCase() + userinput[0].substring(1);
        this.sideOne = Double.parseDouble(userinput[1]);
        this.sideTwo = Double.parseDouble(userinput[2]);
        this.sideThree = Double.parseDouble(userinput[3]);
    }

    public String getName() {
        return this.name;
    }

    public double getSquare() {
        double p = (this.sideOne + this.sideTwo + this.sideThree) / 2;

        double triangleSquare = Math.sqrt(p * (p - this.sideOne) * (p - this.sideTwo) * (p * this.sideThree));
        return triangleSquare;
    }
}
