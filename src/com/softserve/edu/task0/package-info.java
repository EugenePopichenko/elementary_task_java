
/**
 * Contains classes wich are common for all the tasks in "com.softserve.edu".
 *
 * The classes responds for the next functionality:
 * 1. The application should not exit on failure,
 * when the not valid data is given as params on the application startup.
 *
 * 2. The instructions(Help) should be displayed
 * when there no params are given on the application startup.
 */
package com.softserve.edu.task0;
