
package com.softserve.edu.task0;

/**
 * The <code>Helpable</code> interface should be implemented by any
 * class whose instances are intended to
 * display the instructions(Help)
 * when there no params are given on the application startup.
 * <p>
 * The instantianted class must override the <code>display()</code> method.
 */
public interface Helpable {
    /**
     * Displays the instructions(Help)
     * when there no params are given on the application startup.
     * Must be overrident in the instantianted class.
     */
    void displayHelp();
}
