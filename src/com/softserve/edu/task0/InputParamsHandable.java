
package com.softserve.edu.task0;

import com.softserve.edu.task1.model.ChessBoardAppExceptions.ChessBoardAppException;

/**
 * The <code>InputParamsHandable</code> interface should be implemented by any
 * class whose instances are intended to
 * handle the reaction of the application on
 * the not valid data is given as params on the application startup.
 * <p>
 * The instantianted class must override the <code>validate()</code> method.
 * The instantianted class must override the <code>handle()</code> method.
 */
public interface InputParamsHandable {
    /**
     * Defines if the valid data was given as params on the application startup.
     *
     * @param args - a String array arguments of to validate
     * @return <code>true</code> if the params are valid,
     * <code>false</code> if the params are invalid.
     * @throws ChessBoardAppException if args are not valid
     */
    boolean validate(String[] args) throws ChessBoardAppException;

    /**
     * Defines the way the application behave on
     * the not valid data is given as params on the application startup.
     * <p>
     * Exit the application if
     * the <code>isValid</code> is <code>false</code>
     *
     * @param isValid - a String array arguments of to handle.
     */
    void handle(boolean isValid);
}
