
package com.softserve.edu.task6_DSerdiuk.calculator;

import com.softserve.edu.task6_DSerdiuk.Ticket;
import com.softserve.edu.task6_DSerdiuk.generator.TicketGenerator;

/**
 * @author Dmytro Serdiuk
 */
public class TicketsCalculator {

    private final TicketGenerator ticketGenerator;
    private final HappyTicketCalculator happyTicketCalculator;

    public TicketsCalculator(TicketGenerator ticketGenerator, HappyTicketCalculator happyTicketCalculator) {
        this.ticketGenerator = ticketGenerator;
        this.happyTicketCalculator = happyTicketCalculator;
    }

    public void calculate() {
        int all = 0, happy = 0;

        while (ticketGenerator.hasNext()) {
            Ticket ticket = ticketGenerator.next();
            boolean isHappy = happyTicketCalculator.isHappy(ticket);
            all++;
            if (isHappy) happy++;
        }
        System.out.println(String.format("%s tickets were calculated by %s method. There are %s happy tickets!",
                all, happyTicketCalculator.methodName(), happy)
        );
    }
}
