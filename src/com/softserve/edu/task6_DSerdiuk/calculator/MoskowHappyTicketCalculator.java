package com.softserve.edu.task6_DSerdiuk.calculator;

import com.softserve.edu.task6_DSerdiuk.Ticket;

/**
 * @author Dmytro Serdiuk
 */
public class MoskowHappyTicketCalculator implements HappyTicketCalculator {

    @Override
    public String methodName() {
        return "Moscow";
    }

    @Override
    public boolean isHappy(Ticket ticket) {
        Integer[] number = ticket.getNumber();
        return number[0] + number[1] + number[2] == number[3] + number[4] + number[5];
    }
}
