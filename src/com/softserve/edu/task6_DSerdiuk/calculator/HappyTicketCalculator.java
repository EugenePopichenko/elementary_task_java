package com.softserve.edu.task6_DSerdiuk.calculator;

import com.softserve.edu.task6_DSerdiuk.Ticket;

/**
 * @author Dmytro Serdiuk
 */
public interface HappyTicketCalculator {

    String methodName();

    boolean isHappy(Ticket ticket);
}
