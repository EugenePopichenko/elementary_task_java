package com.softserve.edu.task6_DSerdiuk;

import com.softserve.edu.task6_DSerdiuk.calculator.HappyTicketCalculator;
import com.softserve.edu.task6_DSerdiuk.calculator.MoskowHappyTicketCalculator;
import com.softserve.edu.task6_DSerdiuk.calculator.PiterHappyTicketCalculator;
import com.softserve.edu.task6_DSerdiuk.calculator.TicketsCalculator;
import com.softserve.edu.task6_DSerdiuk.generator.TicketGenerator;
import com.softserve.edu.task6_DSerdiuk.input.InputReader;

public class App {

    public static void main(String[] args) {
        String method = new InputReader().getCalculationMethod(args);

        if ("Moskow".equalsIgnoreCase(method)) {
            calculate(new MoskowHappyTicketCalculator());
        } else if ("Piter".equalsIgnoreCase(method)) {
            calculate(new PiterHappyTicketCalculator());
        } else {
            System.out.println("Provided calcluation method is unknown: " + method);
        }
    }

    private static void calculate(HappyTicketCalculator happyTicketCalculator) {
        TicketGenerator ticketGenerator = new TicketGenerator();
        new TicketsCalculator(ticketGenerator, happyTicketCalculator).calculate();
    }
}
