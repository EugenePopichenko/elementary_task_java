package com.softserve.edu.task6_DSerdiuk.input;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author Dmytro Serdiuk
 */
public class InputReader {

    private static final String INSTRUCTION = "Please provide path to file with a method for calculation " +
            "when run a program as an arguments. File has to contain 'Moskow' or 'Piter' word at the first line.";

    public String getCalculationMethod(String[] args) {
        if (args.length == 0) {
            System.out.println(INSTRUCTION);
            System.exit(0);
        }
        String path = args[0];
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            String method = reader.readLine().trim();
            if (method.isEmpty()) {
                System.out.println("Input file is empty. Location: " + path);
            } else {
                return method;
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println("Error access file: " + e.getMessage());
        }
        System.exit(-1);
        return null;
    }
}
