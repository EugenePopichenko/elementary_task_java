package com.softserve.edu.task5;

public class ParamsValidator {

    public static String validate(String[] args) {
        // Help, no params.
        if (args.length == 0) {
            System.out.println("\n***Help***\ngive one integer as a param, to run the App");
            System.exit(0);
        }

        // more than one params
        if (args.length > 1) {
            System.out.println("\n***Wrong number of params***\ngive the ONE integer as a param, to run the App");
            System.exit(0);
        }

        // if isInteger
        try {
            Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            System.out.println("\n***NumberFormatException***\ngive one INTEGER as a param, to run the App");
            System.exit(0);
        }

        return args[0];
    }
}
