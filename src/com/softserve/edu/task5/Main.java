package com.softserve.edu.task5;

/**
 * Created by Eugene_P on 08.01.2016.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println(NumberToWords.convertToEng(Integer.parseInt(ParamsValidator.validate(args))));
    }
}
