package com.softserve.edu.task5;

/**
 * Created by User on 08.01.2016.
 */
public class NumberToWords {

    private static final String[] zero = {"zero"};

    private static final String[] units = {"", " one", " two", " three", " four", " five", " six", " seven", " eight", " nine"};

    private static final String[] teens = {" ten", " eleven", " twelve", " thirteen", " fourteen", " fifteen", " sixteen", " seventeen", " eighteen", " nineteen"};

    private static final String[] tens = {"", " ten", " twenty", " thirty", " forty", " fifty", " sixty", " seventy", " eighty", " ninety"};


    public static String convertToEng(int intNum) {
        String sign = "";
        String strNum = null;
        if (intNum < 0) {
            sign = "minus _";
            intNum *= -1;
        }
        if (intNum < 100) {
            strNum = convertUpToHundred(intNum);
        } else if (intNum < 1_000) {
            strNum = convertHundreds(intNum);
        } else if (intNum < 1_000_000) {
            strNum = convertThousands(intNum);
        }


        return (strNum == null) ? "Not realised yet" : sign + strNum;
    }

    private static String convertUnits(int intNum) {
        return units[intNum / 1];
    }

    private static String convertTeens(int intNum) {
        return teens[intNum % 10];
    }

    private static String convertTens(int intNum) {
        return tens[intNum / 10];
    }

    private static String convertUpToHundred(int intNum) {
        String strNum = null;
        if (0 == intNum) {
            strNum = zero[intNum];
        }

        if ((0 < intNum) && (intNum <= 9)) {
            strNum = convertUnits(intNum);
        }

        if ((10 <= intNum) && (intNum <= 19)) {
            strNum = convertTeens(intNum);
        }

        if (20 <= intNum) { // && (intNum <= 99)
            int tensInt = intNum - (intNum % 10);
            int unitsInt = intNum % 10;
            strNum = convertTens(tensInt) + convertUnits(unitsInt);
        }
        return strNum;
    }

    private static String convertHundreds(int intNum) {
        String strNum = "";

        int hundredsInt = (intNum - (intNum % 100)) / 100;
        int upToHundredInt = (intNum % 100);

        if (hundredsInt != 0) {
            strNum += convertUpToHundred(hundredsInt) + " hundred";
        }

        if (upToHundredInt != 0) {
            strNum += convertUpToHundred(upToHundredInt);
        }
        return strNum;
    }

    private static String convertThousands(int intNum) {
        String strNum = "";

        int thousandsInt = (intNum - (intNum % 1000)) / 1000;
        int upToThousandInt = (intNum % 1000);

        if (thousandsInt != 0) {
            strNum += convertHundreds(thousandsInt) + " thousand";
        }
        if (upToThousandInt != 0) {
            strNum += " _" + convertHundreds(upToThousandInt);
        }
        return strNum;
    }
}
