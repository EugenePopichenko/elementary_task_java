package com.softserve.edu.task2;

import java.util.Scanner;

/**
 * Envelope fabrick.
 * creates an envelope with the parameters
 * recieved from user while creating
 */
public final class EnvelopeCreator {
    /**
     * Suppresses the default constructor.
     */
    private EnvelopeCreator() {
    }

    /**
     * imitates command-line dialogue with user.
     * to determine the sides for the envelipe
     *
     * @return envelope - array of double with length of 2,
     * where the elements represents the value of the envelope sides
     */
    public static double[] askSizeAndCreateEnvelope() {

        double[] envelope = new double[2];

        System.out.println("height:");
        envelope[0] = askForValidInput();

        System.out.println("width:");
        envelope[1] = askForValidInput();

        return envelope;
    }

    /**
     * imitates command-line dialogue with user.
     * to get the double from the command-line window
     *
     * @return num - parsed to double and validated user input.
     */
    private static double askForValidInput() {
        Scanner scn = new Scanner(System.in);
        String scannedStr = scn.nextLine();

        double num = 0;
        // isDouble isBigger than 0(zero)
        try {
            num = Double.parseDouble(scannedStr);
            if (num <= 0) {
                throw new NumberFormatException();
            }
        } catch (NumberFormatException e) {
            System.out.println("*NumberFormatException*");
            scannedStr = "";
        }

        while (scannedStr.length() == 0) {
            System.out.println("Hey! type some positive,"
                    + " bigger than \"0\" numeric!");
            scannedStr = scn.nextLine();
            try {
                // isDouble isBigger than 0(zero)
                num = Double.parseDouble(scannedStr);
                if (num <= 0) {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                scannedStr = "";
            }
        }
        return num;
    }
}
