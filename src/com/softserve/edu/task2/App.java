package com.softserve.edu.task2;

import java.util.Scanner;

/**
 * "Envelope analysis" application.
 * Shows if the one envelope can be placed in the another
 */
public final class App {
    /**
     * Suppresses the default constructor.
     */
    private App() {
    }

    /**
     * Entry poinr.
     *
     * @param args - an array of String arguments
     *             read from the command-line input.
     *             converted from command-line arguments,
     *             which were entered and specified
     *             while invoking the application
     *             after the name of the class to be run
     */
    public static void main(final String[] args) {

        boolean toContinue = true;

        while (toContinue) {
            System.out.println("\nPlease, enter sides for the first envelope");
            double[] envelopeOne = EnvelopeCreator.askSizeAndCreateEnvelope();
            System.out.println("Please enter sides for the second envelope");
            double[] envelopeTwo = EnvelopeCreator.askSizeAndCreateEnvelope();

            System.out.println(
                    EnvelopeAnalyzer.
                            analyzeAndGiveAnswer(envelopeOne, envelopeTwo));

            toContinue = App.askForContinuing();
        }
    }

    /**
     * imitates command-line dialogue with user.
     * to determine if user wants to continue
     *
     * @return true - if user wants to continue;
     * false - if user does not wants to continue
     */
    private static boolean askForContinuing() {
        boolean toContinue;
        final String continueRequest =
                "Please, type \"Yes\" or \"Y\" to continue..."
                        + "otherwise, the App will exit";
        System.out.println(continueRequest);
        Scanner scn = new Scanner(System.in);
        String scannedLine = scn.nextLine().trim();
        if (scannedLine.equalsIgnoreCase("yes")
                || scannedLine.equalsIgnoreCase("y")) {
            toContinue = true;
        } else {
            toContinue = false;
        }
        return toContinue;
    }
}
