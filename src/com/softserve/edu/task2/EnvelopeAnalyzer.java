
package com.softserve.edu.task2;

/**
 * Analisys if the one envelope can be placed into the another.
 */
public final class EnvelopeAnalyzer {
    /**
     * Suppresses the default constructor.
     */
    private EnvelopeAnalyzer() {
    }

    /**
     * Analisys if the one envelope can be placed into the another.
     * and vice versa
     *
     * @param envelopeOne - array of bouble with length of two,
     *                    where the parameters stands for envelope sides
     * @param envelopeTwo - array of bouble with length of two,
     *                    where the parameters stands for envelope sides
     * @return String representadion/description
     * if the one envelope can be placed into the another.
     */
    public static String analyzeAndGiveAnswer(
            final double[] envelopeOne, final double[] envelopeTwo) {

        return ifOneCanBePlacedInAnother(envelopeOne, envelopeTwo) + "\n"
                + ifOneCanBePlacedInAnother(envelopeTwo, envelopeOne);
    }


    /**
     * Analisys if the one envelope can be placed into the another.
     *
     * @param envOne - array of bouble with length of two,
     *               where the parameters stands for envelope sides
     * @param envTwo - array of bouble with length of two,
     *               where the parameters stands for envelope sides
     * @return String representadion/description
     * if the one envelope can be placed into the another.
     */
    private static String ifOneCanBePlacedInAnother(
            final double[] envOne, final double[] envTwo) {

        String rezStr = null;

        // height of envOne
        double envOneHeight = (envOne[0] < envOne[1]) ? envOne[0] : envOne[1];
        // width of envOne
        double envOneWidth = (envOne[0] > envOne[1]) ? envOne[0] : envOne[1];
        // diagonal of envOne
        double envOneDiagonal = Math.sqrt(Math.pow(envOneHeight, 2)
                + Math.pow(envOneWidth, 2));

        // height of envTwo
        double envTwoHeight = (envTwo[0] < envTwo[1]) ? envTwo[0] : envTwo[1];
        // width of envTwo
        double envTwoWidth = (envTwo[0] > envTwo[1]) ? envTwo[0] : envTwo[1];
        // diagonal of envTwo
        double envTwoDiagomal = Math.sqrt(Math.pow(envTwoHeight, 2)
                + Math.pow(envTwoWidth, 2));

// equal
        if ((envOneHeight == envTwoHeight) && (envOneWidth == envTwoWidth)) {
            rezStr = "Envelopes are equal";
        }

// parallel inserting
        if (envOneHeight > envTwoHeight) {
            rezStr = envOne[0] + ":" + envOne[1]
                    + " envelope DO NOT can be inserted into "
                    + envTwo[0] + ":" + envTwo[1] + " envelope";
        }

        if (envOneHeight < envTwoHeight) {
            if (envOneWidth < envTwoWidth) {
                rezStr = envOne[0] + ":" + envOne[1]
                        + " envelope DO can be inserted into "
                        + envTwo[0] + ":" + envTwo[1] + " envelope";
            } else if (envOneWidth > envTwoWidth) {
// not parallel inserting
                if (envOneDiagonal > envTwoDiagomal) {
                    rezStr = envOne[0] + ":" + envOne[1]
                            + " envelope DO NOT can be inserted into "
                            + envTwo[0] + ":" + envTwo[1] + " envelope";
                }

                // for more details see
                // "olimpiadnie zadachi po programmirovaniu"
                // by Fedor Menshikov 2006, page 128-131.
                double cornerHoMinCoHoMax =
                        2 * Math.atan(envOneHeight / envOneWidth);
                double cornerAOB = Math.atan(
                        (Math.sqrt(Math.pow(envOneDiagonal / 2, 2)
                                - Math.pow(envTwoHeight / 2, 2)))
                                / (envTwoHeight / 2));
                double cornerCOD = Math.atan(
                        (Math.sqrt(Math.pow(envOneDiagonal / 2, 2)
                                - Math.pow(envTwoWidth / 2, 2)))
                                / (envTwoWidth / 2));
                double cornerBOC = 90 - cornerAOB - cornerCOD;

                if (cornerHoMinCoHoMax < cornerBOC) {
                    rezStr = envOne[0] + ":" + envOne[1]
                            + " envelope DO can be inserted into "
                            + envTwo[0] + ":" + envTwo[1] + " envelope";
                }
            }
        }
        return rezStr;
    }
}
