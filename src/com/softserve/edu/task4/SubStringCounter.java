package com.softserve.edu.task4;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User on 10.01.2016.
 */
public class SubStringCounter {
    public static String count(String path, String strToCount) {
        int numOfStringsToCount = 0;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(path)))) {

            String readLine;

            while ((readLine = bufferedReader.readLine()) != null) {

                if (readLine.contains(strToCount)) {
                    Pattern pattern = Pattern.compile(strToCount);
                    Matcher matcher = pattern.matcher(readLine);
                    while (matcher.find()) {
                        numOfStringsToCount++;
                    }
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "There are " + numOfStringsToCount + " given strings in the file";
    }
}
