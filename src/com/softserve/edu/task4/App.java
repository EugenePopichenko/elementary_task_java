package com.softserve.edu.task4;

import java.io.IOException;

/**
 * Created by User on 10.01.2016.
 */
public class App {
    public static void main(String[] args) throws IOException {
        ParamsValidator.validate(args);

//         1.	Считать количество вхождений строки в текстовом файле. <путь к файлу> <строка для подсчёта>
        if (args.length == 2) {
            System.out.println(SubStringCounter.count(args[0], args[1]));
        }

//         2.	Делать замену строки на другую в указанном файле. <путь к файлу> <строка для поиска> <строка для замены>
        if (args.length == 3) {
            StringRplacer.replace(args[0], args[1], args[2]);
        }
    }
}
