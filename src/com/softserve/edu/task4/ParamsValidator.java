package com.softserve.edu.task4;

import java.io.File;

public class ParamsValidator {

    public static void validate(String[] stringArgs) {
        // Help, no params.
        if (stringArgs.length == 0) {
            System.out.println("\n===Help===\n" +
                    "call the \"com.softserve.edu.task4.App xxxx yyyy\".\n" +
                    "where \"xxxx\" is a string path to file;\n" +
                    "      \"yyyy\" is a string to count presence in file of;\n" +
                    "\n" +
                    "or call the \"com.softserve.edu.task4.App xxxx yyyy zzzz\".\n" +
                    "where \"xxxx\" is a string path to file;\n" +
                    "      \"yyyy\" is a string to search in file for;\n" +
                    "      \"zzzz\" is a string to replace on");
            System.exit(0);
        }

        // more than two params
        if (stringArgs.length > 3) {
            System.out.println("\n===Wrong number of params===\n" +
                    "give the TWO or THREE string params, to run the App\n" +
                    "for Help call the \"com.softserve.edu.task4.App\"");
            System.exit(0);
        }

        // if the first param is a valid path to file
        File file = new File(stringArgs[0]); // NullPointerException should have been excluded in "Help part"
        if (!file.exists() && !file.isFile()) {
            System.out.println("\n==Wrong fileName==\ntry to run the App one more time");
            System.exit(0);
        }
    }
}
