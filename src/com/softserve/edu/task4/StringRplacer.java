package com.softserve.edu.task4;

import java.io.*;

/**
 * Created by User on 10.01.2016.
 */
//// only for files with less than MAX_STRING_LENGTH number of chars
//// read the whole file into string than rewrite file - not good
//public class StringRplacer {
//    public static void replace(String path, String toReplace, String replacedOn) throws IOException {
//        String content = "";
//        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
//            String s;
//            while ((s = reader.readLine()) != null) {
//                content+=s+"\n";
//            }
//            content = content.replaceAll(toReplace, replacedOn);
//        }
//        try(BufferedWriter writer = new BufferedWriter(new FileWriter(path))){
//            writer.write(content);
//        }
//    }


public class StringRplacer {
    public static void replace(String path, String toReplace, String replacedOn) throws IOException {

        File file = new File(path);
        String canonicalPath = file.getCanonicalPath();

        File tempFile = new File(canonicalPath + ".~tempFile");


        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {


            String s;
            while ((s = reader.readLine()) != null) {
                s = s.replaceAll(toReplace, replacedOn);

                try (BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile, true))) {
                    writer.write(s);
                    writer.newLine();
                }
            }


        }

        file.delete();

        tempFile.renameTo(file.getAbsoluteFile());

    }


}
