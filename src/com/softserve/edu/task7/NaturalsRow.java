package com.softserve.edu.task7;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by User on 09.01.2016.
 */
public class NaturalsRow {
    public static List<Integer> generate(int upToHere) {

        List<Integer> naturalsList = new LinkedList<>();

        for (int i = 0; Math.pow(i, 2) < Math.sqrt(upToHere); i++) {
            naturalsList.add(i);
        }

        return naturalsList;
    }
}
