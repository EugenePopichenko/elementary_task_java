package com.softserve.edu.task7;

import java.util.List;

/**.
 * for (int i = 0; Math.pow(i, 2) < Math.sqrt(upToHere); i++) {
 *      naturalsList.add(i);
 * }
 */
public class App {
    public static void main(String[] args) {
        List<Integer> naturalsRow = NaturalsRow.generate(ParamsValidator.validate(args));

        String naturalsRowStr = naturalsRow.toString();
        System.out.println(naturalsRowStr.substring(1, naturalsRowStr.length() - 1));
    }
}
