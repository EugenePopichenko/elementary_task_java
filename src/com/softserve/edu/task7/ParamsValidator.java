package com.softserve.edu.task7;

public class ParamsValidator {

    public static int validate(String[] stringArgs) {
        // Help, no params.
        if (stringArgs.length == 0) {
            System.out.println("\n===Help===\n" +
                    "call the \"com.softserve.edu.task7.App x\".\n" +
                    "where \"x\" is an integer, number which power of two will be the argument to generate naturals row relatively\n");
            System.exit(0);
        }

        // more than one params
        if (stringArgs.length != 1) {
            System.out.println("\n===Wrong number of params===\n" +
                    "give the ONE integer as a params, to run the App.\n" +
                    "a number which power of two will be the argument to generate naturals row relatively\n");
            System.exit(0);
        }

        // if isInteger and positive and not zero
        int[] intArgs = new int[1];
        try {
            intArgs[0] = Integer.parseInt(stringArgs[0]);
            if (intArgs[0] < 0) {
                throw new NumberFormatException();
            }
            if (intArgs[0] == 0) {
                throw new ArithmeticException();
            }
        } catch (NumberFormatException e) {
            System.out.println("\n***NumberFormatException***\ngive the POSITIVE INTEGER as a param, to run the App");
            System.exit(0);
        } catch (ArithmeticException e) {
            System.out.println("\n***ArithmeticException***\nthe \"zero\" is the smallest natural number");
            System.exit(0);
        }

        return intArgs[0];
    }
}
