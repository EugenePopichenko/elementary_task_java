
package com.softserve.edu.task1b;


import com.softserve.edu.task1b.inrterfaces.AppExecutor;
import com.softserve.edu.task1b.inrterfaces.Helper;
import com.softserve.edu.task1b.inrterfaces.InputParamsHandler;
import com.softserve.edu.task1b.inrterfaces.InputParamsValidator;

/**
 * "ChessBoard" application.
 * Displays the String representation of the chessboard.
 * <p>
 * Where the "*" is used to display the dark square
 * and the " " is used to display the light square.
 * <p>
 * The number of squares of horizontal and vertical sides of the chessboard
 * is determined by the arguments which are given as params
 * on the application startup.
 */
public final class App {
    /**
     * Suppresses the default constructor.
     */
    private App() {
    }

    /**
     * Entry point.
     *
     * @param args - an array of Strings.
     *             Converted from the command-line arguments,
     *             which were specified on the application startup.
     */
    public static void main(final String[] args) {

        // overrideing
        Helper chessBoardHelper = (helpMessage) -> {
            System.out.println(helpMessage);
        };

        InputParamsValidator chessBoardValidator = (argsToValidate) -> {
            if (argsToValidate.length == 0) {
                return 0;
            }

            for (String arg : argsToValidate) {
                try {
                    if (Integer.parseInt(arg) <= 0) {
                        throw new IllegalArgumentException(
                                String.valueOf(Message.NOT_A_POSITIVE_INTEGER));
                    }
                } catch (NumberFormatException e) {
                    System.out.println(Message.WRONG_NUMBER_FORMAT);
                    System.exit(-1);
                } catch (IllegalArgumentException e) {
                    System.out.println(e.getMessage());
                    return -1;
                }
            }

            try {
                if (argsToValidate.length != 2) {
                    throw new ArrayIndexOutOfBoundsException(
                            String.valueOf(Message.WRONG_NUMBER_OF_ARGUMENTS));
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println(e.getMessage());
                return -1;
            }
            return 1;
        };

        AppExecutor chessBoardExecutor = (argsToExecute) -> {
            String chessBoardString = "";
            boolean dark = true;
            for (int i = 1; i <= Integer.parseInt(argsToExecute[1]); i++) {

                if (i % 2 == 0) {
                    dark = false;
                }
                for (int j = 0; j < Integer.parseInt(argsToExecute[0]); j++) {
                    if (dark) {
                        chessBoardString += "*";
                        dark = false;
                    } else {
                        chessBoardString += " ";
                        dark = true;
                    }
                }
                chessBoardString += "\n";

                if (Integer.parseInt(argsToExecute[0]) % 2 == 0) {
                    dark = !dark;
                }
            }
            System.out.println(chessBoardString);
        };

        InputParamsHandler handler = (helper, validator, executor) -> {
            int validationIndicator = validator.validate(args);
            if (validationIndicator == 0) {
                helper.showHelp(String.valueOf(Message.HELP));
            } else if (validationIndicator == 1) {
                executor.execute(args);
            } else if (validationIndicator == -1) {
                System.exit(-1);
            }
        };


        // running
        handler.handle(
                chessBoardHelper,
                chessBoardValidator,
                chessBoardExecutor
        );

    }
}
