package com.softserve.edu.task1b.inrterfaces;

/**
 * Created by Eugene_P on 2016-01-18.
 */
public interface Helper {
    /**
     * defines the Help.
     *
     * @param helpMessage help txt
     */
    void showHelp(String helpMessage);
}
