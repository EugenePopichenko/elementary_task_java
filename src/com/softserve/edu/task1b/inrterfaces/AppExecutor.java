
package com.softserve.edu.task1b.inrterfaces;

/**
 * Created by Eugene_P on 2016-01-18.
 */
public interface AppExecutor {
    /**
     * the very main App functionality.
     * @param args income App functionality data
     */
    void execute(String[] args);
}
