package com.softserve.edu.task1b.inrterfaces;

/**
 * Created by Eugene_P on 2016-01-18.
 */
public interface InputParamsHandler {
    /**
     * @param helper    helper
     * @param validator validator
     * @param executor  executor
     */
    void handle(
            Helper helper,
            InputParamsValidator validator,
            AppExecutor executor);
}
