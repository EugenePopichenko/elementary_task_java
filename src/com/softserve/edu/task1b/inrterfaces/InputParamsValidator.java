package com.softserve.edu.task1b.inrterfaces;

/**
 * Created by Eugene_P on 2016-01-18.
 */
public interface InputParamsValidator {
    /**
     * @param args income params to validate
     * @return "0" - if (args.length==0);
     * "1" - if args are valid;
     * "-1" - if args are not valid.
     */
    int validate(String[] args);
}
