
package com.softserve.edu.task1b;

/**
 * Created by Eugene_P on 2016-01-16.
 */
public enum Message {
    /**
     * The message to be displayed as a Help.
     * when there no params are given on the application startup.
     */
    HELP {
        @Override
        public String toString() {
            return "***HELP***\n"
                    + "call the \"com.softserve.edu.task1.App x y\".\n"
                    + "where \"x\" is an integer, number of rows;\n"
                    + "      \"y\" is an integer, number of columns;\"";
        }
    },

    /**
     * The message to be displayed
     * when the WrongNumberOfArgumentsException occurred.
     */
    WRONG_NUMBER_OF_ARGUMENTS {
        @Override
        public String toString() {
            return "***WRONG_NUMBER_OF_ARGUMENTS***\n"
                    + "give the TWO integer as a params, to run the App\n";
        }
    },

    /**
     * The message to be displayed
     * when the NotAPositiveIntegerException occurred.
     */
    NOT_A_POSITIVE_INTEGER {
        @Override
        public String toString() {
            return "***NOT_A_POSITIVE_INTEGER***\n"
                    + "give the two BIGGER THAN \"0\" integer "
                    + "as params, to run the App";
        }
    },

    /**
     * The message to be displayed
     * when the WrongNumberFormatException occurred.
     */
    WRONG_NUMBER_FORMAT {
        @Override
        public String toString() {
            return "***WRONG_NUMBER_FORMAT***\n"
                    + "give the two positive INTEGER as params, to run the App";
        }
    }
}
