package com.softserve.edu.task6;

/**
 * Created by User on 11.01.2016.
 */
public class LuckyTicket {
    public static int countLuckyTicketsFromZeroTo999999(String algorithmType) {
        int luckyTickets = 0;
        if (algorithmType.equals("moskow")) {
            luckyTickets = moskowLuckyTicketsCount();
        } else if (algorithmType.equals("piter")) {
            luckyTickets = piterLuckyTicketsCount();
        }
        return luckyTickets;
    }

    private static int moskowLuckyTicketsCount() {
        int start = 999;
        int end = 999_999;
        int count = 0;
        for (int i = start; i <= end; i++) {
            int first = (i / 100_000) + (i % 100_000 / 10_000) + (i % 10_000 / 1_000);
            int second = (i % 1_000 / 100) + (i % 100 / 10) + (i % 10);
            if (first == second) {
                count++;
            }
        }
        return count;
    }

    private static int piterLuckyTicketsCount() {
        int start = 0;
        int end = 999_999;

        int count = 0;
        for (int i = start; i <= end; i++) {

            int even = 0;
            int odd = 0;

            int xOO_OOO = (i / 100_000);
            int OxO_OOO = (i % 100_000 / 10_000);
            int OOx_OOO = (i % 10_000 / 1_000);
            int OOO_xOO = (i % 1_000 / 100);
            int OOO_OxO = (i % 100 / 10);
            int OOO_OOx = (i % 10);


            even += (xOO_OOO % 2 == 0) ? 1 : 0;
            even += (OxO_OOO % 2 == 0) ? 1 : 0;
            even += (OOx_OOO % 2 == 0) ? 1 : 0;
            even += (OOO_xOO % 2 == 0) ? 1 : 0;
            even += (OOO_OxO % 2 == 0) ? 1 : 0;
            even += (OOO_OOx % 2 == 0) ? 1 : 0;

            odd += (xOO_OOO % 2 != 0) ? 1 : 0;
            odd += (OxO_OOO % 2 != 0) ? 1 : 0;
            odd += (OOx_OOO % 2 != 0) ? 1 : 0;
            odd += (OOO_xOO % 2 != 0) ? 1 : 0;
            odd += (OOO_OxO % 2 != 0) ? 1 : 0;
            odd += (OOO_OOx % 2 != 0) ? 1 : 0;

            if (even == odd) {
                count++;
            }
        }

        return count;
    }
}
