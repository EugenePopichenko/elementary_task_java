package com.softserve.edu.task6;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User on 11.01.2016.
 */
public class AlgorithmType {
    public static String getType(String[] args) throws FileNotFoundException {

        String algorithmType = null;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(args[0])))) {


            String readLine;
            while ((readLine = bufferedReader.readLine()) != null) {
                readLine = readLine.toLowerCase();
                if (readLine.contains("moskow")) {
                    Pattern pattern = Pattern.compile("moskow");
                    Matcher matcher = pattern.matcher(readLine);
                    while (matcher.find()) {
                        algorithmType = "moskow";
                    }
                } else if (readLine.contains("piter")) {
                    Pattern pattern = Pattern.compile("piter");
                    Matcher matcher = pattern.matcher(readLine);
                    while (matcher.find()) {
                        algorithmType = "piter";
                    }
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return algorithmType;
    }
}
