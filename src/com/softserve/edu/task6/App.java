package com.softserve.edu.task6;

import java.io.FileNotFoundException;

/**
 * Created by User on 11.01.2016.
 */
public class App {
    public static void main(String[] args) {
        try {
            System.out.println(LuckyTicket.countLuckyTicketsFromZeroTo999999(AlgorithmType.getType(ParamsValidator.validate(args))));
        } catch (FileNotFoundException e) {
            System.out.println("There was no \"Moskow\" or \"Piter\" in file");
            System.exit(0);
        }
    }
}
