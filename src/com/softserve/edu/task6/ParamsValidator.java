package com.softserve.edu.task6;

import java.io.File;

public class ParamsValidator {

    public static String[] validate(String[] stringArgs) {
        // Help, no params.
        if (stringArgs.length == 0) {
            System.out.println("\n===Help===\n" +
                    "call the \"com.softserve.edu.task6.App xxxx\".\n" +
                    "where \"xxxx\" is a string path to file.\n");
            System.exit(0);
        }

        // more than two params
        if (stringArgs.length > 1) {
            System.out.println("\n===Wrong number of params===\n" +
                    "give the ONE string param, to run the App\n" +
                    "for Help call the \"com.softserve.edu.task6.App\"");
            System.exit(0);
        }

        // if the first param is a valid path to file
        File file = new File(stringArgs[0]); // NullPointerException should have been excluded in "Help part"
        if (!file.exists() && !file.isFile()) {
            System.out.println("\n==Wrong fileName==\ntry to run the App one more time");
            System.exit(0);
        }
        return stringArgs;
    }
}
