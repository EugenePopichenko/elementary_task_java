
package com.softserve.edu.task1;

import com.softserve.edu.task0.Helpable;
import com.softserve.edu.task0.InputParamsHandable;
import com.softserve.edu.task1.controller.ChessBoard;
import com.softserve.edu.task1.model.ChessBoardAppExceptions.ChessBoardAppException;
import com.softserve.edu.task1.model.ChessBoardAppExceptions.NotAPositiveIntegerException;
import com.softserve.edu.task1.model.Message;
import com.softserve.edu.task1.view.Input;
import com.softserve.edu.task1.view.Output;

/**
 * "ChessBoard" application.
 * Displays the String representation of the chessboard.
 * <p>
 * Where the "*" is used to display the dark square
 * and the " " is used to display the light square.
 * <p>
 * The number of squares of horizontal and vertical sides of the chessboard
 * is determined by the arguments which are given as params
 * on the application startup.
 */
public final class App {
    /**
     * Suppresses the default constructor.
     */
    private App() {
    }

    /**
     * Entry point.
     *
     * @param args an array of Strings.
     *             Converted from the command-line arguments,
     *             which were specified on the application startup.
     */
    public static void main(final String[] args) {

        try {
            if (args.length == 0) {
                Helpable help = () -> {
                    System.out.println(Message.HELP);
                };
                help.displayHelp();
            } else {
                InputParamsHandable input = new Input();
                boolean isValid = input.validate(args);
                input.handle(isValid);

                ChessBoard chessBoard = new ChessBoard(args);
                Output output = new Output();
                output.displayChessBoard(chessBoard);
            }
        } catch (ChessBoardAppException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }
}
