package com.softserve.edu.task1.view;

import com.softserve.edu.task1.controller.ChessBoard;
import com.softserve.edu.task1.controller.ChessBoardToStringParser;

/**
 * Created by Eugene_P on 2016-01-16.
 */
public class Output {
    /**
     * Displays the ChessBoard object.
     *
     * @param chessBoard the ChessBoard to be displayed.
     */
    public final void displayChessBoard(final ChessBoard chessBoard) {
        System.out.println(new ChessBoardToStringParser(chessBoard).
                getStringRepresentationOfChessBoard());
    }
}
