
package com.softserve.edu.task1.view;

import com.softserve.edu.task0.InputParamsHandable;
import com.softserve.edu.task1.model.ChessBoardAppExceptions.ChessBoardAppException;
import com.softserve.edu.task1.model.ChessBoardAppExceptions.NotAPositiveIntegerException;
import com.softserve.edu.task1.model.ChessBoardAppExceptions.WrongNumberFormatException;
import com.softserve.edu.task1.model.ChessBoardAppExceptions.WrongNumberOfArguments;
import com.softserve.edu.task1.model.Message;

/**
 * Created by Eugene_P on 2016-01-16.
 */
public class Input implements InputParamsHandable {

    // Client-side validation
    @Override
    public final boolean validate(final String[] args)
            throws ChessBoardAppException {

        boolean isValid = true;
        // WRONG_NUMBER_OF_ARGUMENTS
        if (args.length != 2) {
            isValid = false;
            throw new WrongNumberOfArguments(
                    Message.WRONG_NUMBER_OF_ARGUMENTS);
        }

        // WRONG_NUMBER_FORMAT
        // NOT_A_POSITIVE_INTEGER
        for (int i = 0; i < 2; i++) {
            int j = 0;
            try {
                j = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                isValid = false;
                throw new WrongNumberFormatException(
                        Message.WRONG_NUMBER_FORMAT);
            }
            if (j <= 0) {
                isValid = false;
                throw new NotAPositiveIntegerException(
                        Message.NOT_A_POSITIVE_INTEGER);
            }
        }
        return isValid;
    }

    @Override
    public final void handle(final boolean isValid) {
        if (!isValid) {
            System.exit(1);
        }
    }
}
