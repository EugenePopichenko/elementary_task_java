
package com.softserve.edu.task1.model.ChessBoardAppExceptions;

import com.softserve.edu.task1.model.Message;

/**
 * Created by Eugene_P on 2016-01-16.
 */
public class ChessBoardAppException extends Exception {
    /**
     * Constructs a new exception with the specified detail message.  The
     * cause is not initialized, and may subsequently be initialized by
     * a call to {@link #initCause}.
     *
     * @param message the detail message. The detail message is saved for
     *                later retrieval by the {@link #getMessage()} method.
     */
    public ChessBoardAppException(final Message message) {
        super(String.valueOf(message));
    }
}
