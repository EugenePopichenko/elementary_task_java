
package com.softserve.edu.task1.model;

/**
 * The representation of the chessboard sqares.
 */
public enum Square {
    /**
     * Character to represent a light square of the chessboard.
     */
    LIGHT {
        @Override
        public String toString() {
            return " ";
        }
    },
    /**
     * Character to represent a dark square of the chessboard.
     */
    DARK {
        @Override
        public String toString() {
            return "*";
        }
    };
}
