
package com.softserve.edu.task1.controller;


import com.softserve.edu.task1.model.Square;

/**
 * The representation of a chessboard.
 */
public class ChessBoard {

    /**
     * number of squares on horizontal side of ChessBoard.
     */
    private int horizontalSquareNumber = 0;
    /**
     * number of squares on vertical side of ChessBoard.
     */
    private int verticalSquareNumber = 0;
    /**
     * the dark square of the chessboard.
     */
    private Square darkSquare = Square.DARK;
    /**
     * the light square of the chessboard.
     */
    private Square lightSquare = Square.LIGHT;

    /**
     * Constructor/Setter.
     * defines number of rows and lines for the ChessBoard
     *
     * @param chessBoardSize - an array of Strings, where
     *                       the first element stands for number of
     *                       squares on horizontal side of ChessBoard,
     *                       the first element stands for number of
     *                       squares on vertical side of ChessBoard.
     */
    public ChessBoard(final String[] chessBoardSize) {
        int hs = (Integer.parseInt(chessBoardSize[0]));
        int vs = (Integer.parseInt(chessBoardSize[1]));
        if ((hs >= 0) && (vs >= 0)) {
            this.horizontalSquareNumber = hs;
            this.verticalSquareNumber = vs;
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Getter.
     * Gets the number of squares on horizontal side of the ChessBoard.
     *
     * @return this.horizontalSquareNumber: int
     */
    public final int getHorizontalSquareNumber() {
        return this.horizontalSquareNumber;
    }
    /**
     * getter.
     * Gets the number of squares on vertical side of the ChessBoard.
     *
     * @return this.verticalSquareNumber: int
     */
    public final int getVerticalSquareNumber() {
        return this.verticalSquareNumber;
    }
    /**
     * getter.
     * Gets the dark square of the chessboard.
     *
     * @return this.darkSquare: Square
     */
    public final Square getDarkSquare() {
        return this.darkSquare;
    }
    /**
     * getter.
     * Gets the light square of the chessboard.
     *
     * @return this.lightSquare: Square
     */
    public final Square getLightSquare() {
        return this.lightSquare;
    }
}
