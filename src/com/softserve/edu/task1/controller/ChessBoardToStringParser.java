package com.softserve.edu.task1.controller;

/**
 * Parses the ChessBoard object to a String representation of it.
 * The object content is defined by an array of int
 * given as a parameter to the constructor
 */
public class ChessBoardToStringParser {

    /**
     * The string representation of the ChessBoard object.
     */
    private String stringRepresentationOfChessBoard = "";

    /**
     * Constructor/Setter.
     *
     * @param chessBoard the ChessBoard object to be parsed to String.
     */
    public ChessBoardToStringParser(final ChessBoard chessBoard) {

        boolean dark = true;
        for (int i = 1; i <= chessBoard.getVerticalSquareNumber(); i++) {

            if (i % 2 == 0) {
                dark = false;
            }
            for (int j = 0; j < chessBoard.getHorizontalSquareNumber(); j++) {
                if (dark) {
                    this.stringRepresentationOfChessBoard
                            += chessBoard.getDarkSquare();
                    dark = false;
                } else {
                    this.stringRepresentationOfChessBoard
                            += chessBoard.getLightSquare();
                    dark = true;
                }
            }
            this.stringRepresentationOfChessBoard += "\n";

            if (chessBoard.getHorizontalSquareNumber() % 2 == 0) {
                dark = !dark;
            }
        }
    }

    /**
     * Getter.
     *
     * @return this.stringRepresentationOfChessBoard: String
     */
    public final String getStringRepresentationOfChessBoard() {
        return this.stringRepresentationOfChessBoard;
    }
}
